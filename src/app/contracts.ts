declare let require: any;

const canyaAbi = require('assets/abi/canyaABI.json');
const daoAbi = require('assets/abi/daoABI.json');
const canworkAbi = require('assets/abi/can-work-job.abi.json');
const priceOracleAbi = require('assets/abi/ERC20BancorPriceOracle.json').abi;

export {
    canyaAbi,
    daoAbi,
    canworkAbi,
    priceOracleAbi
};
